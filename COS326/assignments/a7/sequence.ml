open System
open Future 
open Mpi


module type S = sig
  type 'a t
  val tabulate : (int -> 'a) -> int -> 'a t
  val seq_of_array : 'a array -> 'a t
  val array_of_seq : 'a t -> 'a array
  val iter: ('a -> unit) -> 'a t -> unit
  val length : 'a t -> int
  val empty : unit  ->'a t
  val cons : 'a -> 'a t -> 'a t
  val singleton : 'a -> 'a t
  val append : 'a t -> 'a t -> 'a t
  val nth : 'a t -> int -> 'a
  val map : ('a -> 'b) -> 'a t -> 'b t
  val map_reduce : ('a -> 'b) -> ('b -> 'b -> 'b) -> 'b -> 'a t -> 'b
  val reduce : ('a -> 'a -> 'a) -> 'a -> 'a t -> 'a
  val flatten : 'a t t -> 'a t
  val repeat : 'a -> int -> 'a t
  val zip : ('a t * 'b t) -> ('a * 'b) t
  val split : 'a t -> int -> 'a t * 'a t
  val scan: ('a -> 'a -> 'a) -> 'a -> 'a t -> 'a t
end



(*******************************************************)
(* Sequential Sequences Based on a List Representation *)
(*******************************************************)

module ListSeq : S = struct

  type 'a t = 'a list

  let length = List.length

  let empty () = []

  let cons (x:'a) (s:'a t) = x::s

  let singleton x = [x]

  let append = List.append

  let tabulate f n =
    let rec helper acc x =
      if x = n then List.rev acc
      else helper ((f x)::acc) (x+1) in
    helper [] 0

  let nth = List.nth

  let filter = List.filter

  let map = List.map

  let reduce = List.fold_left

  let map_reduce m r b s = reduce r b (map m s)

  let repeat x n =
    let rec helper x n acc =
      if n = 0 then acc else helper x (n-1) (x::acc) in
    helper x n []

  let flatten = List.flatten

  let zip (s1,s2) = List.combine s1 s2

  let split s i =
    let rec helper s i acc =
      match s,i with
        | [],_ -> failwith "split"
        | _,0 -> (List.rev acc,s)
        | h::t,_ -> helper t (i-1) (h::acc) in
    helper s i []

  let iter = List.iter

  let array_of_seq = Array.of_list

  let seq_of_array = Array.to_list

  let scan f b s = 
    let (_,xs) = List.fold_left (fun (v,ls) e -> let r = f v e in (r,r::ls)) (b,[]) s in
    List.rev xs

end


(*******************************************************)
(* Parallel Sequences                                  *)
(*******************************************************)

module type SEQ_ARGS = sig 
  val use_mpi: bool
end


module Seq (Par : Future.S) (Arg : SEQ_ARGS) : S = struct

  type 'a t = 'a array


  let num_cores = System.cpu_count ()
  let default = 50000
  let ratio = 2

  type interval_message = Interval of int * int | Kill
 
  (* How do I make a receive_message type that is the same as 'a t*)

  let tabulate f n =
    (*minimum chunk size proportional to the number of cores*)
    (*let min_chunk = max default (n / (ratio * num_cores)) in*)
    let min_chunk = max 1 (n/num_cores) in
    (*handler*)
    let rec handler ch () = 
      let rec aux () =
        match Mpi.receive ch with
            Kill -> ()
          | Interval (lo,hi) when (hi - lo + 1 <= min_chunk) -> (*create the result if chunk is too small*)
            let arr = (Array.init (hi - lo + 1) (fun i -> f (lo + i))) in
            Mpi.send ch arr; aux () (*send the chunk and kill this process*)
          | Interval (lo,hi) -> (*sub-divide into two smaller chunks*)
            let mid = ((hi + lo) / 2) in
            let l = Mpi.spawn handler () in
            let r = Mpi.spawn handler () in 
            (*delegate two chunks to subprocess*)
            Mpi.send l (Interval (lo,mid));
            Mpi.send r (Interval (mid + 1,hi));
            (*get the answers*)
            let l_answer = Mpi.receive l in
            let r_answer = Mpi.receive r in
            let _ = Mpi.send l Kill, Mpi.send r Kill in
            (*let them die*)
            Mpi.wait_die l;Mpi.wait_die r; 
            let result = Array.append l_answer r_answer in
            Mpi.send ch result; aux () (*we die ourselves*)
      in aux ()
    in
    match n, (n >= 0) with
      _, false -> raise (Invalid_argument "index out of bounds")
    | 0, _ -> [||]
    | _, _ -> (let ch = Mpi.spawn handler () in
    Mpi.send ch (Interval (0,n - 1));
    let answer = Mpi.receive ch in
    let _ = Mpi.send ch Kill in
    let _ = Mpi.wait_die ch in
    answer)
      
          


  let seq_of_array a = a


  let array_of_seq seq = seq


  let iter f seq = 
    Array.iter f seq


  let length seq = Array.length seq


  let empty () = [||]


  let cons elem seq = Array.append [|elem|] seq


  let singleton elem = [|elem|]


  let append seq1 seq2 = Array.append seq1 seq2


  let nth seq i = Array.get seq i

  type 'a receive_message = Seq of 'a t | Kill

  (*parallel*)
  let map f seq = 
    (*let min_chunk = max default ((Array.length seq) / (ratio * num_cores)) in*)
    let min_chunk = max 1 (Array.length seq)/(num_cores) in
    let rec handler ch () =
      let rec aux () =
        match Mpi.receive ch with
          Kill -> ()
        | Seq s -> 
          let n = length s in
          if (n <= min_chunk) then (Mpi.send ch (Array.map f s); aux ())
          else 
          begin
            let mid = n / 2 in
            let l,r = Mpi.spawn handler (), Mpi.spawn handler () in
            Mpi.send l (Seq (Array.sub s 0 mid));Mpi.send r (Seq (Array.sub s mid (n - mid)));
            let l_ans, r_ans = Mpi.receive l, Mpi.receive r in
            let _ = Mpi.send l Kill,Mpi.send r Kill in
            Mpi.wait_die l;Mpi.wait_die r;
            let result = append l_ans r_ans in
            Mpi.send ch result; aux ()
          end
      in aux ()
    in 
    let ch = Mpi.spawn handler () in
    Mpi.send ch (Seq seq);
    let ans = Mpi.receive ch in
    let _ = Mpi.send ch Kill in
    let _ = Mpi.wait_die ch in
    ans


  (*parallel*)
  let map_reduce m r b seq = 
    (*let min_chunk = max default ((Array.length seq) / (ratio * num_cores)) in*)
    let min_chunk = max 1 (Array.length seq)/num_cores in
    let rec handler ch () = 
      let rec aux () = 
 	match Mpi.receive ch with
          Kill -> ()
        | Seq s -> 
          let n = length s in
          if (n <= min_chunk) then (Mpi.send ch (Array.fold_left  r (m s.(0)) (Array.map m (Array.sub s 1 (n - 1)))); aux ())
            else begin
            let mid = n / 2 in
            let l,r' = Mpi.spawn handler(), Mpi.spawn handler () in
            Mpi.send l (Seq (Array.sub s 0 mid));Mpi.send r' (Seq (Array.sub s mid (n - mid)));
            let l_ans, r_ans = Mpi.receive l, Mpi.receive r' in
            let _ = Mpi.send l Kill, Mpi.send r' Kill in
            Mpi.wait_die l;Mpi.wait_die r';
            let result = (r l_ans r_ans) in
            Mpi.send ch result; aux ()
         end
      in aux ()
    in
    match seq with
      [||] -> b
    | _ -> 
      let ch = Mpi.spawn handler () in
      Mpi.send ch (Seq seq);
      let ans = Mpi.receive ch in
      let _ = Mpi.send ch Kill in
      let _ = Mpi.wait_die ch in    
      (r ans b) 


  (*parallel*)
  let reduce r = fun u seq ->
    (*let min_chunk = max default ((Array.length seq) / (ratio *num_cores)) in*)
    let min_chunk = max 1 (Array.length seq)/num_cores in
    let rec handler ch () =
      let rec aux () =
        match Mpi.receive ch with
          Kill -> ()
        | Seq s ->
          let n = length s in
          match (n <= min_chunk, n) with
          | true, _ -> Mpi.send ch (Array.fold_left r s.(0) (Array.sub s 1 (n - 1)));aux ()
          | false, _ ->
            let mid = n / 2 in
            let l, r' = Mpi.spawn handler (), Mpi.spawn handler () in
            Mpi.send l (Seq (Array.sub s 0 mid));Mpi.send r' (Seq (Array.sub s mid (n - mid)));
            let l_ans, r_ans = Mpi.receive l, Mpi.receive r' in
            let _ = Mpi.send l Kill, Mpi.send r' Kill in
            Mpi.wait_die l;Mpi.wait_die r';
            let result = (r l_ans r_ans) in
            Mpi.send ch result; aux ()
     in aux ()
    in
    match seq with
      [||] -> u
    | _ -> 
        let ch = Mpi.spawn handler () in
        Mpi.send ch (Seq seq);
        let ans = Mpi.receive ch in
        let _ = Mpi.send ch Kill in
        let _ = Mpi.wait_die ch in
        (r u ans)

  (*seq of sequences*)
  let flatten seqseq = Array.fold_left (fun acc seq -> Array.append acc seq) [||] seqseq
         
  (*sequential*)
  let repeat elem num = Array.make num elem

  (*sequential*)
  let zip (seq1,seq2) = 
    let n = min (length seq1) (length seq2) in
    let rec pair_seq i acc = 
      if (i = n) then acc
      else pair_seq (i + 1) (append acc (singleton (nth seq1 i,nth seq2 i)))
    in pair_seq 0 [||]

  (*sequential*)
  let split seq x = 
    let len = length seq in
    if (len < x) then failwith "error usage" 
    else (Array.sub seq 0 x,Array.sub seq x (len - x))


  (*******************************************************)
  (* Parallel Prefix Sum                                 *)
  (*******************************************************)

  (* Here you will implement a version of the parallel prefix scan for a sequence 
   * [a0; a1; ...], the result of scan will be [f base a0; f (f base a0) a1; ...] *)

  type 'a compute_message = Seq of 'a t | FromLeft of 'a | Kill
  type 'a result_message = Seq of 'a t | Up of 'a 

  let scan (f: 'a -> 'a -> 'a) (base: 'a) (seq: 'a t) : 'a t=
    let min_chunks = max 1 (length seq)/num_cores in
    let rec handler (ch:('a result_message,'a compute_message) Mpi.channel) () =
      let rec aux () =
        match Mpi.receive ch with
          Kill -> ()
        | Seq seq -> 
          (
            let n = length seq in
            match (n, n <= min_chunks) with
              0,_ -> failwith "impossible"
            | 1,_ | _, true -> 
                  let rec prefix_sum prev curr arr  = 
                    if (curr = length arr) then ()
                    else (arr.(curr) <- (f (arr.(curr)) prev);prefix_sum (arr.(curr)) (curr + 1) arr)
                  in
                  Mpi.send ch (Up (Array.fold_left f seq.(0) (Array.sub seq 1 (n - 1))));
                  (match Mpi.receive ch with
                    FromLeft fl -> (prefix_sum fl 0 seq;Mpi.send ch (Seq seq);aux ())
                  | Seq _ | Kill -> failwith "wrong input OR premature Kill")
            | _,_ -> 
                 let mid = n / 2 in
                 let l : ('a compute_message, 'a result_message) Mpi.channel = Mpi.spawn handler () in
                 let r : ('a compute_message, 'a result_message) Mpi.channel = Mpi.spawn handler () in
                 Mpi.send l (Seq (Array.sub seq 0 mid));Mpi.send r (Seq (Array.sub seq mid (n - mid)));
                 match Mpi.receive l, Mpi.receive r with
                   Up v1, Up v2 ->
                     begin
                       Mpi.send ch (Up (f v1 v2));
                       match Mpi.receive ch with
                         FromLeft fl ->
                           begin
                             Mpi.send l (FromLeft fl);Mpi.send r (FromLeft (f fl v1));
                             match Mpi.receive l, Mpi.receive r with
                               Seq s1, Seq s2 -> Mpi.send ch (Seq (append s1 s2));Mpi.send l Kill;Mpi.send r Kill;Mpi.wait_die l;Mpi.wait_die r;aux ()
                             | _,_ -> failwith "wrong scope"
                           end
                       | _ -> failwith "wrong scope0"
                     end
                 | _,_ -> failwith "wrong scope1"
         )
        | FromLeft _ -> failwith "not possible2"
      in aux () 
   in match length seq  with
     0 -> [||]
   | _ ->
     let ch = Mpi.spawn handler () in
     Mpi.send ch (Seq seq);
     let _ = Mpi.receive ch in
     let _ = Mpi.send ch (FromLeft base) in
     match Mpi.receive ch with
       Seq seq -> Mpi.send ch Kill;Mpi.wait_die ch;seq
     | _ -> failwith "impossible"
  
end







