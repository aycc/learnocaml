open Future

module PSeq = Sequence.Seq(PFuture)(struct let use_mpi = true end)
module SMap = Map.Make(String)

(* inverted_index computes an inverted index for the contents of
 * a given file. The filename is the given string.
 * The results are output to stdout. *)
let mkindex (args : string ) : unit =
  let file = Util.read_whole_file args in
  let words = Util.split_words file |> List.fold_left (fun acc x -> Array.append acc [|String.lowercase x|]) [||] in
  let index = PSeq.seq_of_array words |> PSeq.map_reduce (fun x -> SMap.singleton x 1) 
                                        (fun xs ys -> 
                                         SMap.merge (fun k xo yo ->
                                                     match xo, yo with 
 						       Some x, Some y -> Some (x + y)
                                                     | None, 	yo ->  yo
                                                     | xo, None -> xo ) xs ys)
                                        SMap.empty
  in
  let output () = SMap.iter (fun k v -> print_string k;print_string ": ";print_int v;print_string "\n") index in
  output ()
	
