
open Util
open Future

type profile = {
  firstname : string;
  lastname : string;
  sex : string;
  age : int;
  lo_agepref : int;
  hi_agepref : int;
  profession : string;
  has_children : bool;
  wants_children : bool;
  leisure : string;
  drinks : bool;
  smokes : bool;
  music : string;
  orientation : string;
  build : string;
  height : string
}

let convert (p : string) : profile =
  let s = String.concat " " (Str.split (Str.regexp_string "@") p) in
  Scanf.sscanf s "%s@ %s@ %s@ %d %d %d %s@ %B %B %s@ %B %B %s@ %s@ %s@ %s"
  (fun firstname lastname sex age lo_agepref hi_agepref profession has_children
       wants_children leisure drinks smokes music orientation build height ->
   { firstname = firstname;
     lastname = lastname;
     sex = sex;
     age = age;
     lo_agepref = lo_agepref;
     hi_agepref = hi_agepref;
     profession = profession;
     has_children = has_children;
     wants_children = wants_children;
     leisure = leisure;
     drinks = drinks;
     smokes = smokes;
     music = music;
     orientation = orientation;
     build = build;
     height = height
   })

let print_profile ({
     firstname = firstname;
     lastname = lastname;
     sex = sex;
     age = age;
     lo_agepref = lo_agepref;
     hi_agepref = hi_agepref;
     profession = profession;
     has_children = has_children;
     wants_children = wants_children;
     leisure = leisure;
     drinks = drinks;
     smokes = smokes;
     music = music;
     orientation = orientation;
     build = build;
     height = height } : profile) : unit =
  Printf.printf "%s %s\n" firstname lastname;
  Printf.printf "  sex: %s  age: %d  profession: %s\n" sex age profession;
  Printf.printf "  %s  %s\n" (if drinks then "social drinker" else "nondrinker") (if smokes then "smoker" else "nonsmoker");
  Printf.printf "  %s  %s\n"
    (if has_children then "has children" else "no children")
    (if wants_children then "wants children" else "does not want children");
  Printf.printf "  prefers a %s partner between the ages of %d and %d\n"
    (if (orientation="straight" && sex="F") || (orientation = "gay/lesbian" && sex="M") then "male" else "female")
    lo_agepref hi_agepref;
  Printf.printf "  likes %s music and %s\n" music leisure


let print_matches (n : string) ((p, ps) : profile * (float * profile) list) : unit =
  print_string "------------------------------\nClient: ";
  print_profile p;
  Printf.printf "\n%s best matches:\n" n;
  List.iter (fun (bci, profile) ->
    Printf.printf "------------------------------\nCompatibility index: %f\n" bci; print_profile profile) ps;
  print_endline ""



module PSeq = Sequence.Seq(PFuture)(struct let use_mpi = true end)

(* apm computes the potential love of your life.  The filename of a file
 * containing candidate profiles is in location 0 of the given array.
 * The number of matches desired is in location 1.  The first and last name
 * of the (human) client are in location 2 and 3, respectively.  The client's
 * profile must be in the profiles file.  Results are output to stdout. *)
let matchme (args : string array) : unit = 
  let desired = (int_of_string args.(1)) in
  let slack = 3 in
  let u = { firstname = ""; lastname = ""; sex = ""; age = 0; lo_agepref = 0; 
            hi_agepref = 0;  profession = ""; has_children = false; wants_children = false;
            leisure = ""; drinks = false;  smokes = false; music = ""; orientation = ""; build = "";
            height = ""} in
  let match_orientation = fun c t ->
    (c.sex = t.sex && c.orientation = t.orientation && c.orientation = "gay/lesbian") ||
    (c.sex <> t.sex && c.orientation = t.orientation && c.orientation = "straight") in

  let match_age  = fun c t ->
    (c.age >= (t.lo_agepref - slack) && c.age <= (t.hi_agepref + slack)) &&
    (t.age >= (t.lo_agepref - slack) && t.age <= (c.hi_agepref +  slack))  in

  let match_profession = fun c t ->
    c.profession = t.profession in

  let match_children = fun c t ->
    (c.wants_children = t.wants_children) ||
    (c.wants_children && t.has_children) ||
    (c.has_children && t.wants_children) ||
    (c.has_children && t.has_children) in

  let match_health = fun c t ->
    ((c.drinks = t.drinks) && (c.smokes = t.smokes)) in
  
  let match_misc = fun c t ->
    (c.music = t.music) || (c.leisure =  t.leisure) in  

  let read_profiles ((firstname,lastname): string * string) (filename: string) :  (profile) * (profile PSeq.t) =
    let f = open_in filename in
    let rec next (p,seq) =
      match (try Some (input_line f) with End_of_file  -> None) with
      | None -> (p,seq)
      | Some line ->
        let profile = convert line in
        if (profile.firstname = firstname && profile.lastname = lastname) then (next (profile, seq))
        else (next (p,PSeq.append seq (PSeq.singleton profile)))
    in next (u, PSeq.empty ())
  in
  let process_profiles (client: profile) (profiles: profile PSeq.t) : (float * profile) PSeq.t = 
    let float_of_bool =  fun b -> match b with true -> 1. | false -> 0. in
    let potentials = PSeq.map_reduce (fun p -> 
                                             let t = (
                                             if (not (match_orientation client p && match_health client p)) then 0.
                                             else ( 0.5 *. (float_of_bool (match_age client p)) +. 0.2 *. (float_of_bool (match_profession client p))
                                             +. 0.2 *. (float_of_bool (match_children client p)) +. 0.1 *. (float_of_bool (match_misc client p))) )
                                             in (PSeq.singleton (t,p) ) )
                                          (fun u v -> PSeq.append u v)
                                          (PSeq.empty ()) profiles |> PSeq.array_of_seq in
    Array.sort (fun (x,_) (y,_) -> compare y x) potentials;
    PSeq.seq_of_array potentials   in
  let filename, matches, firstname, lastname = args.(0), args.(1), args.(2), args.(3) in
  let (client,profiles) = read_profiles (firstname,lastname) filename in  
  let potential_profiles = process_profiles client profiles in 
  let bars = "------------------------------" in
  let (|<) f v = f v in
  print_endline bars;print_string "Client: "; print_profile client;
  print_int desired;print_string " best matches:\n";
  PSeq.array_of_seq potential_profiles |> Array.sub |< 0 |< desired |>
  Array.iter (fun (v,p) -> print_endline bars;print_string "Compatibility index: ";print_float v;print_string "\n";print_profile p)























