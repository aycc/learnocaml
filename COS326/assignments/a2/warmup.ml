(*************)
(* PROBLEM 1 *)
(*************)

(* For each part of problem 1, explain in a comment why the code
   will not typecheck, then follow the instructions for that part to
   change the code so that it typechecks while keeping the same values
   as intended by the erroneous code. Once you have done so, uncomment
   the fixed expression for submission.
*)

(* Problem 1a - fix the right-hand-side of the assignment to match the
   listed type. (Do not change the left-hand-side.)

   Did not typecheck because:   


*)
let prob1a : int list = [1; 2; 3] ;;

(* Problem 1b - fix the type of variable prob1b to match the type of
   the expression on the right-hand-side of the assignment.

   Did not typecheck because:

*)
let prob1b : string * int list = ("COS", [326;441]);;

(* Problem 1c - fix the right-hand-side of the expression to match 
   the variable prob1c's listed type.
   
   Did not typecheck because:


 *)
let prob1c : float list = [2.0; 3.0] @ [4.0 ; 5.0];;

(*************)
(* PROBLEM 2 *)
(*************)

(* Fill in expressions to satisfy the following types: 
 *
 * NOTE: for option and list types, you must go at least one layer deep.
 * example problems:
 *   let x : int option = ???;;
 *   let y : int list =  ???;;
 * incorrect answers:
 *   let x : int option = None ;;
 *   let y : int list = [] ;;
 * possible correct answers:
 *   let x : int option = Some 1 ;;
 *   let y : int list = [1] ;;
 *   let y : int list = [1; 2] ;;
 *   let y : int list = 1 :: [] ;;
 *)

(*>* Problem 2a *>*)
let prob2a : (float * (string * int) option list) list =
	(4.5,[None;Some ("abc",4)]) :: []
;;

(*>* Problem 2b *>*)
(* a student is a (name, age option) pair *)
type student = string * int option;;
let prob2b : (student list option * int) list = 
	(Some [("deedee",None);("jejee",(Some 14))],54) :: []
;;


(*>* Problem 2c *>*)
let prob2c : (int * int -> int) * (float -> float -> unit) * bool  =
  let f_i ((x:int),(y:int)): int = 
	(x + y) in
  let f_f (x:float) (y:float): unit = 
	() in
  (f_i, f_f, true)
;;

(* Fill in ??? with something that makes these typecheck: *)
(*>* Problem 2d *>*)
let prob2d =
  let rec foo bar =
    match bar with
    | (a, (b, c)) :: xs -> if a then (b + c + (foo xs)) else foo xs
    | _ -> 0
  in foo [(true,(1,0));(false,(2,0));(true,(3,0))]
;;

Printf.printf "%d\n" (prob2d)

(*************)
(* PROBLEM 3 *)
(*************)

(* Consider the following terribly written function: *)

let rec zardoz f ls acc =
  if (((List.length ls) = 1) = true) then (f (List.hd(ls)) (acc))
  else if (((List.length ls) = 0) = true) then acc
  else let hd = List.hd(ls) in
  let tl = List.tl(ls) in
  let ans = f (hd) (acc) in
  let ans = zardoz f tl ans in
    ans
;;

(* Rewrite the code above so that it does the same thing
 * but style-wise is far superior.  
 * Write some assert statements
 * to check that your function is doing the same thing as the original.  
 * Use the COS 326 style guide. *)
let rec myzardoz f ls acc = 
	match ls with
		h :: t -> zardoz f t (f h acc)
	       | [] -> acc
;;


(*************)
(* PROBLEM 4 *)
(*************)

(***************************************)
(* Conway's Lost Cosmological Theorem! *)
(***************************************)

(* 

If l is any list of integers, the look-and-say list of s is obtained by 
reading off adjacent groups of identical elements in s. For example, the 
look-and-say list of

l = [2; 2; 2]

is

[3; 2]

because l is exactly "three twos." Similarly, the look-and-say sequence of

l = [1; 2; 2]

is

[1; 1; 2; 2]

because l is exactly "one ones, then two twos."

We will use the term run to mean a maximal length sublist of a list with 
all equal elements. For example,

[1; 1; 1] and [5]

are both runs of the list

[1; 1; 1; 5; 2]

but

[1; 1] and [5; 2] and [1; 2]

are not: 

[1; 1] is not maximal
[5; 2] has unequal elements
[1; 2] is not a sublist.

You will now define a function look and say that computes the 
look-and-say sequence of its argument.  

For full credit your solution should be a linear time solution.

CULTURAL ASIDE:

The title of this problem comes from a theorem about the sequence generated 
by repeated applications of the "look and say" operation. As look and say 
has type int list -> int list, the function can be applied to its own result. 
For example, if we start with the list of length one consisting of just the 
number 1, we get the following first 6 elements of the sequence:

[1]
[1,1]
[2,1]
[1,2,1,1]
[1,1,1,2,2,1]
[3,1,2,2,1,1]

Conway's theorem states that any element of this sequence will "decay" 
(by repeated applications of look and say) into a "compound" made up of 
combinations of "primitive elements" (there are 92 of them, plus 2 
infinite families) in 24 steps. If you are interested in this sequence, 
you may wish to consult [Conway(1987)] or other papers about the 
"look and say" operation.
*)

let look_and_say (xs: int list) : int list = 
  let rec helper xs curr count acc =
    match xs with
      [] -> (curr :: count :: acc)
    | h :: t -> match (h = curr) with
                  true -> helper t curr (count + 1) acc
                | false -> helper t h 1 (curr :: count :: acc)
  in match xs with
    [] -> []
  | h :: t -> helper t h 1 []
;;
(* reverse to fix*)
(*************)
(* PROBLEM 5 *)
(*************)

(* 5a. Write a function that flattens a list of lists in to a single
 * list with all of the elements in order. eg:
 *
 * flatten [[1;2;3]; []; [4]; [5;6]] = [1;2;3;4;5;6] 
 *
 *)


let rec flatten (xss:'a list list) : 'a list = 
  let rec helper ( xss:'a list list) (xs:'a list) (acc:'a list) : 'a list = 
    match xs with
      hs :: ts -> helper xss ts (hs :: acc)
    | [] -> match xss with
              hss :: tss -> (helper tss hss acc)
             | [] -> acc
    in helper xss [] []
;;


(*e 5b. Given a matrix (list of lists), return the transpose.
 * The transpose of a matrix interchanges the rows and columns.
 * For example, transpose [[1;2;3];[4;5;6]];;
 * where [1;2;3] and [4;5;6] are the rows,
 * should return [[1;4];[2;5];[3;6]].

 * raise the exception BadMatrix if the inner lists have uneven length
 * 
 *)

exception BadMatrix
let badMatrix () = raise BadMatrix

let rec transpose (m:int list list) : int list list = match m with
  [] -> []
 |[] :: xss -> transpose xss
 |(x :: xs) :: xss -> 
    (x :: List.map List.hd xss) :: transpose (xs :: List.map List.tl xss)


(* 5c. Return the list of all permutations of the input list. eg: 
 * perm [1;2;3] = [[1;2;3]; [1;3;2]; [2;1;3]; [2;3;1]; [3;1;2]; [3;2;1]] 
*)
(* test this on small inputs!! *)
let perm (items:'a list) : 'a list list =
  failwith "unimplemented"
;;

