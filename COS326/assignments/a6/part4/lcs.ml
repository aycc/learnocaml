open Memoizer
open Timing

type base = Base.base;;
type dna = Base.dna;;

(* slow lcs *)
let rec slow_lcs ((s1,s2) : dna * dna) : dna =
  match (s1,s2) with 
      ([], _) -> []
    | (_, []) -> []
    | (x :: xs, y :: ys) ->
      if Base.eq x y then
	x :: slow_lcs (xs, ys)
      else
	Base.longer_dna_of (slow_lcs (s1, ys)) (slow_lcs (xs, s2))
;;

(* A potentially useful module *)
module DnaPairOrder : Map.OrderedType with type t = dna * dna =
struct
    type t = dna * dna

    let rec compare_dna x' y' : int = 
        match x',y' with 
          [],[] -> 0
        | [], xs -> -1
        | xs, [] -> 1
        | x::xs, y::ys -> 
	  (match Base.compare x y with
	      0 -> compare_dna xs ys
            | other -> other)
	    

    (* implements a lexicographic ordering: 
     * compare the second components only if first components are equal *)
    let compare (a, b) (c, d) =
      match compare_dna a c with
	  0 -> compare_dna b d
        | other -> other
     
end;;

(* Task 4.4 *)

(* implement fast_lcs using your automatic memoizer functor! 
 * doing so will of course require proper creation of modules and
 * use of functors *)

(* convert (key -> 'a) to ((key -> 'a) -> (key -> 'a)) 
 * we need to decompose so fast_lcs calls some recurse function
 *) 
module M = Memoizer(Map.Make(DnaPairOrder))

let fast_lcs (ds : dna * dna) : dna =  
  let lcs_body (recurse: (dna * dna) -> dna) ((s1,s2): dna * dna) : dna =
    match (s1,s2) with
      ([], _) -> []
    | (_,[]) -> []
    | (x :: xs, y :: ys) -> 
      if Base.eq x y then
        x :: (recurse (xs, ys))
      else
        Base.longer_dna_of (recurse (s1,ys)) (recurse (xs,s2))
  in let memo = M.memo lcs_body in
  memo ds
;;

(* Task 4.5 *)

(* Implement some experiment that shows performance difference
 * between slow_lcs and fast_lcs. (Print your results.)     
 * Explain in a brief comment what your experiment shows.        *)
(* main function/testing *)

let print_header () =
  print_string "--------- --------------- Lcs(N) -------------\n";
  print_string "    N     Slow     Automated\n";
  print_string "--------- ------------------------------------\n"
;;

let print_row n slow automated =
  let space () = print_string "   " in
  let float f = Printf.printf "%6.4f" f in
  let print_slow slow =
    match slow with
        None -> print_string "   -  "
      | Some f -> float f
  in
  if n < 10 then print_string " ";
  if n < 100 then print_string " ";
  if n < 1000 then print_string " ";
  if n < 10000 then print_string " ";
  if n < 100000 then print_string " ";
  if n < 1000000 then print_string " ";
  print_int n; space ();
  print_slow slow; space ();
  float automated; space ();
  print_newline()
;;

let experiment ((s1,s2):(dna*dna)) : unit =
  let n = max (String.length (Base.dna_to_string s1)) (String.length (Base.dna_to_string s2)) in
  let slow s = if n > 42 then None else Some (time_fun slow_lcs s) in
  let automated =
    time_fun fast_lcs
  in
  print_row n (slow (s1,s2)) (automated (s1,s2))
;;

let main () =
  (* change these numbers if you want depending on the speed of your machine *)
  (* on my machine slow_fib starts taking visible time at input 30 *)
  let trials = [
               (Base.dna_from_string "AGTGCTGAAAGTTGCGCCAGTGAC",Base.dna_from_string "AGTGCTGAAGTTCGCCAGTTGACG");
               (Base.dna_from_string "CACAATTTTTCCCAGAGAGA", Base.dna_from_string "CGAATTTTTCCCAGAGAGA")] in
  print_header();
  List.iter experiment trials
;;


(* uncomment this block to run tests,
 * but please do not submit with it uncommented
 *)
main ();;
