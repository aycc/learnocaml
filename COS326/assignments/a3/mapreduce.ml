(* 

Name:
Email:
Minutes Spent on Problem 1.1:
Minutes Spent on Problem 1.2:

(You aren't in any way graded on the number of minutes spent; 
 we are just trying to calibrate for future versions of the class)

Comments/Problems/Thoughts on this part of the assignment:

*)

(* This part of the assignment uses the following functions 
 * If you forget how they work, look them up:
 * http://caml.inria.fr/pub/docs/manual-ocaml/libref/List.html
 *)

let map : ('a -> 'b) -> 'a list -> 'b list = List.map;;

let filter : ('a -> bool) -> 'a list -> 'a list = List.filter;;

let foldr : ('a -> 'b -> 'b) -> 'a list -> 'b -> 'b = List.fold_right;;

let foldl : ('b -> 'a -> 'b) -> 'b -> 'a list -> 'b = List.fold_left;;

(* reduce is equivalent to List.fold_right, 
 * only its args are ordered differently *)
let rec reduce (f:'a -> 'b -> 'b) (u:'b) (xs:'a list) : 'b =
  match xs with
    | [] -> u
    | hd::tl -> f hd (reduce f u tl) ;;

(***********************************************)
(******            1.1 WARM UP            ******)
(***********************************************)

(* Solve each problem in this part using map, reduce, foldl, foldr or filter.
 * Map, filter, reduce, foldl, foldr are an example of a *combinator library*:
 * a library of higher-order functions used to solve problems in a particular
 * domain.  In this case, the domain is list-processing.  However, there are 
 * countless other useful combinator libraries.  The goal is to get used to 
 * thinking about how to decompose complex functions in to smaller, simpler, 
 * orthogonal functional components.  The smaller, simpler functional 
 * components can be constructed directly using the combinator library.
 *
 * Note: foldl is slightly more efficient than foldr because it is tail
 * recursive.  (We will explain what that means later in the course.)
 * Hence, solutions that use foldl are typically superior to solutions
 * that use foldr, all other things being equal.  Thus you should try to 
 * use foldl where you can instead of foldr (but take care to retain good 
 * style -- a horribly convoluted, incomprehensible function that uses 
 * foldl is worse than an elegant one that uses foldr).
 *
 * In these problems, you are not allowed to use the "rec" keyword in 
 * your solution.  A solution, even a working one, that uses explicit 
 * recursion via "rec" will receive little to no credit.  You may write
 * useful auxiliary functions; they just may not be recursive.
 *
 * You are also not allowed to use other functions from the list library
 * such as sort, concat or flatten.  (You are allowed to recode those
 * functions yourself using map, filter, fold if you find it necessary.)
 *
 *)

(*>* Problem 1.1.a *>*)

(*  negate_all : Flips the sign of each element in a list *)
let negate_all (nums:int list) : int list =
  map (fun (x:int) -> -1 * x) nums
;;

(* Unit test example.  Uncomment after writing negate_all *)
assert ((negate_all [1; -2; 0]) = [-1; 2; 0]);;


(*>* Problem 1.1.b *>*)

(*  sum_rows : Takes a list of int lists (call an internal list a "row").
 *             Returns a one-dimensional list of ints, each int equal to the
 *             sum of the corresponding row in the input.
 *   Example : sum_rows [[1;2]; [3;4]] = [3; 7] *)
let sum_rows (rows:int list list) : int list =
  map (reduce (+) 0) rows
;;

assert ((sum_rows [[1;2]; [3;4]]) = [3;7]);;


(*>* Problem 1.1.c *>*)

(*  limit_range : Returns a list of numbers in the input list within a
 *                  given range (inclusive), in the same order they appeared
 *                  in the input list.
 *       Example : limit_range [1;3;4;5;2] (1,3) = [1;3;2] *)
let limit_range (nums:int list) (range:int * int) : int list =
  let (x,y) = range in 
  filter (fun i -> (i >= x) && (i <= y )) nums
;;

assert ((limit_range [1;3;4;5;2] (1,3)) = [1;3;2]);;


(*>* Problem 1.1.d *>*)

(*  num_occurs : Returns the number of times a given number appears in a list.
 *     Example : num_occurs 4 [1;3;4;5;4] = 2 *)
let num_occurs (n:int) (nums:int list) : int =
  let same = (filter (fun i -> i = n) nums) in
  List.length same
;;

assert ((num_occurs 4 [1;3;4;5;4]) = 2);;


(*>* Problem 1.1.e *>*)

(*  super_sum : Sums all of the numbers in a list of int lists
 *    Example : super_sum [[1;2;3];[];[5]] = 11 *)
let super_sum (nlists:int list list) : int =
  let partials = map (reduce (+) 0) nlists
  in reduce (+) 0 partials
;;

assert((super_sum [[1;2;3];[];[5]]) = 11);;


(****************************************************)
(**********       1.2 A Bigger Challenge   **********)
(****************************************************)

(*
 * Note: some of these questions may be challenging.  
 * Don't neglect Part 2 of this assignment because you are working on
 * these problems.
 *)

(*>* Problem 1.2.a *>*)

(* min2: returns the second-smallest element of a list, when put into 
 * sorted order. Note that if list contains duplicates, the second-smallest 
 * element and the smallest element may be identical; your code should return 
 * it.
 *
 * Example: min2 [2110; 4820; 3110; 4120] = 3110.
 * Example: min2 [2110; 4820; 2110; 4120] = 2110.
 *
 * For full credit, use a fold function, do not sort the list and do not
 * use the rec keyword (aside from using folds). 
 *
 * You will receive partial credit if you use explicit recursion instead of
 * a fold.
 *
 * If the list contains 0 or 1 elements, call (invalid_arg s) with a helpful
 * string s. See the Pervasives library for the invalid_arg function and 
 * other useful exception-raising functions:
 *
 * http://caml.inria.fr/pub/docs/manual-ocaml/libref/Pervasives.html
*)


let min2 (xs:int list) : int = 
  let selector l c  = 
    match l with
      c2 :: c1 :: [] -> (match (c < c2, c < c1) with
 			 (_,true) -> [c1;c]
			| (true,false) -> [c;c1]
			| (false, false) -> [c2;c1])
      | _ -> failwith "Not implemented"
  in match xs with
    a :: b :: c -> let res = (foldl selector (if (a < b) then [b;a] else [a;b]) c) in 
    (match res with x::y -> x | _ -> failwith "Not implemented")
  | _ -> invalid_arg "list is not of length >= 2"
							
;;

assert ((min2 [2110; 4820; 3110; 4120]) = 3110);;

(*>* Problem 1.2.b *>*)

(* consec_dedupe : removes consecutive duplicate values from a list. 
 * More specifically, consec_dedupe has two arguments:
 *  eq is a function equiv representing an equivalence relation
 *  xs is a list of elements. 
 * It returns a list containing the same elements as lst, but without 
 * any duplicates, where two elements are considered equal if applying eq 
 * to them yields true.
 *
 * Example: consec_dedupe (=) [1; 1; 1; 3; 4; 4; 3] = [1; 3; 4; 3].
 *
 * Example: 
 *
 * let nocase_eq (s1:string) (s2:string) : bool =
 *   (String.uppercase s1) = (String.uppercase s2)
 * ;;
 * 
 * consec_dedupe nocase_eq ["hi"; "HI"; "bi"] = ["hi"; "bi"]
 *  
 * (When consecutive duplicates are not exactly syntactically equal
 * as above, it does not matter which of the duplicates are discarded.)
 *
 * Again, for full credit, do not use explicit recursion (the rec keyword),
 * but instead use foldr or foldl (or both).
 *
 * Partial credit will be given to solutions that do use explicit recursion.
 *)

let consec_dedupe (eq:'a -> 'a -> bool) (xs:'a list) : 'a list =
  let f acc elem = 
    match acc with
      h :: t when h != elem -> elem :: acc
    | h :: t -> acc
    | [] -> [elem]
  in List.rev (foldl f [] xs)
;;

assert ((consec_dedupe (fun (x:int) (y:int) -> x = y) [1;1;1;3;4;4;3]) = [1;3;4;3]);;

(*>* Problem 1.2.c *>*)

(* prefixes: return a list of all non-empty prefixes of a list, 
 * ordered from shortest to longest.

    Example: prefixes [1;2;3;4] = [[1]; [1;2]; [1;2;3]; [1;2;3;4]].
*)

let prefixes (xs: 'a list) : 'a list list =
  let f acc elem = 
    match acc with 
      [] -> [[elem]]
    | h :: t -> (elem :: h) :: acc
  in let partial = foldl f [] xs in
  List.rev (map (List.rev) partial)
;;

assert ((prefixes [1;2;3;4]) = [[1]; [1;2]; [1;2;3]; [1;2;3;4]]);;

(*>* Problem 1.2.d *>*)

(* k_sublist : Given a list of integers nums and an integer k, 
 * the function k_sublist computes the contiguous sublist of length k 
 * whose elements have the largest sum.

    Example: k_sublist [1; 2; 4; 5; 8] 3 = [4; 5; 8].

 * raise invalid_arg "k_sublist" if the length of nums is less than k
 *)

let k_sublist (nums: int list) (k: int) : int list =
  let rec n_forward (nums: int list) (k: int) (acc: int list): int list = 
    match (nums, k) with
	  (_,0) -> List.rev acc
	| (h::t,_) -> n_forward t (k - 1) (h :: acc)
	| ([],_) -> []
  in
  let max_sublist (best: int * (int list)) (xs: int list) : int * (int list) =
    match (best, xs) with
	  _, [] -> best
	| (a,[]), h :: t -> ((List.fold_left (+) 0 xs),xs)
	| (a,b), h :: t -> let s = (List.fold_left (+) 0 xs) in if (s > a) then (s,xs) else best
  in
  let rec get_sublists (acc: (int list) list) (xs: int list) (k:int) : (int list) list = 
    match xs with
	  [] -> List.rev acc
	| h :: t -> get_sublists ((n_forward xs  k []) :: acc) t k
  in let res = (foldl max_sublist (0,[]) (get_sublists [] nums k)) in
  match res with
    (a,b) -> b
  (* not implementing less than k blah blah *)
;;

assert ((k_sublist [1;2;4;5;8] 3) = [4;5;8]);;

(*>* Problem 1.2.e *>*)
(* flatten : write a function that flattens a list of lists in to a single
 * list with all of the elements in order. eg:
 *
 * flatten [[1;2;3]; []; [0]; [4;5]] == [1;2;3;0;4;5] 
 *
 * In the last assignment you wrote this function with recursion. Now, 
 * do it without recursion, using folds.
 *)

let flatten (xss:'a list list) : 'a list = 
  let outer_flatten (acc:'a list) (input:'a list) : 'a list = 
    match input with
	  [] -> acc
	| h :: t -> foldl (fun iacc elem -> elem :: iacc) acc input
  in List.rev (foldl outer_flatten [] xss)
;;

assert ((flatten [[1;2;3]; []; [0]; [4;5]]) = [1;2;3;0;4;5]);;
