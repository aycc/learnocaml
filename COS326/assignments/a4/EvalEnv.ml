(*************************************************)
(* An environment-based evaluator for Dynamic ML *)
(*************************************************)

open Syntax
open Printing
open EvalUtil

(* Defines the subset of expressions considered values
   Notice that closures are values but the rec form is not -- this is
   slightly different from the way values are defined in the 
   substitution-based interpreter.  Rhetorical question:  Why is that?
   Notice also that Cons(v1,v2) is a value (if v1 and v2 are both values).
*) 
let rec is_value (e:exp) : bool = 
  match e with
      Constant _ -> true  
    | Pair (e1, e2) -> is_value e1 && is_value e2
    | EmptyList -> true
    | Cons (e1, e2) -> is_value e1 && is_value e2
    | Closure _ -> true
    | _ -> false

(* evaluation; use eval_loop to recursively evaluate subexpressions *)
let eval_body (env:env) (eval_loop:env -> exp -> exp) (e:exp) : exp = 
  match e with
    | Var x -> 
      (match lookup_env env x with 
  	  None -> raise (UnboundVariable x)
	| Some v -> v)
    | Constant _ -> e
    | Op (e1,op,e2) -> 
        let v1 = eval_loop env e1 in
        let v2 = eval_loop env e2 in
          apply_op v1 op v2
    | If (e1,e2,e3) ->
        (match eval_loop env e1 with
           | Constant (Bool true) -> eval_loop env e2
           | Constant (Bool false) -> eval_loop env e3
           | v1 -> raise (BadIf v1))
    | Let (x,e1,e2) -> let env = (update_env env x e1) in (eval_loop env e2)
    | Pair (e1,e2) -> Pair (eval_loop env e1, eval_loop env e2)
    | Fst e1 -> eval_loop env e1
    | Snd e1 -> eval_loop env e1
    | EmptyList -> EmptyList
    | Cons(e1,e2) -> Cons(eval_loop env e1,eval_loop env e2)
    | Match(e1,e2,hd,tl,e3) -> failwith "unimplemented" (*HOW?*)
    | Rec _ -> e
    | App (e1,e2) ->
           (match eval_loop env e1 with
             (Rec (f,x,body)) as recf ->
               let e2' = eval_loop env e2 in 
               let env' = update_env env x e2'  in 
               let env'' = update_env env' f recf in 
               eval_loop env'' body
            | v1 -> raise (BadApplication v1))
    | Closure (e1,v1,v2,e2) -> failwith "unimplemented" (*TBD
          (match eval_loop e1 *)
        

(* evaluate closed, top-level expression e *)

let eval e =
  let rec loop env e = eval_body env loop e in
  loop empty_env e


(* print out subexpression after each step of evaluation *)
let debug_eval e = 
  let rec loop env e =
    if is_value e then e  (* don't print values *)
    else 
      begin
	Printf.printf "Evaluating %s\n" (string_of_exp e); 
	let v = eval_body env loop e in 
	Printf.printf 
	  "%s evaluated to %s\n" (string_of_exp e) (string_of_exp v); 
	v
      end
  in
  loop empty_env e
